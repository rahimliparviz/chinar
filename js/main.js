$(document).ready(function () {
    $("li.drop_down .no_link").click(function (e) {
        e.preventDefault()
    })
    $("header .drop_down").click(function (event) {
        event.stopPropagation();
        $(this).find(".sub_menu").fadeToggle(500);
        $(this).siblings(".drop_down").find(".sub_menu").fadeOut(300)
    })
    // $(".sub_menu").click(function(event){
    //     event.stopPropagation();
    // })
    $("body").click(function () {
        $("header .sub_menu").fadeOut(300);
    })

    $(".lang .down").click(function () {
        $(".lang_list").slideToggle(400)
    })

    $("#respon_header .burger").click(function () {
        $(".respon_menu").addClass("left_0");

    })
    $(".respon_menu .close_menu").click(function () {
        $(".respon_menu").removeClass("left_0");
    })
    $(".respon_menu .drop_down").click(function () {
        $(this).find(".sub_menu").slideToggle(300)
    })

    $(".tab_panel .tab_floor_1 p").click(function () {
        $(this).addClass("active");
        $(".tab_panel .tab_floor_2 p").removeClass("active");
    })
    $(".tab_panel .tab_floor_2 p").click(function () {
        $(this).addClass("active");
        $(".tab_panel .tab_floor_1 p").removeClass("active");
    })

    $(".tab_floor_1").click(function () {
        $(".floor_1").fadeIn(800);
        $(".floor_2").fadeOut(600);
    })

    $(".tab_floor_2").click(function () {
        $(".floor_2").fadeIn(600);
        $(".floor_1").fadeOut(800);
    })

    $("#events .event_item .desc .view a").click(function (event) {
        event.preventDefault();
        $(this).parent().parent().parent().find(".popup").fadeIn(600);
    })
    $(".event_item .popup .close_pop").click(function () {
        $(".popup").fadeOut(800)
    });


    $('.cal_date').datetimepicker({
        format: 'DD/MM/YYYY'
    });

    $('.cal_time').datetimepicker({
        format: 'LT'
    });
    $("#services .serv_item .item .photo").click(function () {
        $(this).parent(".item").find(".pop_menu").fadeIn(600)
    })

    $(".pop_menu .close_pop").click(function () {
        $(".pop_menu").fadeOut(600);
    });

    $(".close_thank").click(function () {
        $("#thank").fadeOut(400);
    });


    $('#menu_sld').owlCarousel({
        center: true,
        items: 4,
        loop: true,
        margin: 20,
        responsive: {
            0: {
                items: 2
            },
            767: {
                items: 4
            },
        }
    });
    $("#menu_sld .menu_item").click(function () {
        $("#menu_sld").find(".cavid").removeClass("cavid")
        $(this).addClass("cavid");

    })

    //open Gallery slider//
    $('#bambo .photo_list').click(function () {
        $('#gallery_hide a').first().trigger('click');
    });
    ///////////////////////////////////////////

    //Call gallery slider//
    $('#gallery_hide').photobox('a', {
        thumbs: false
    });



///////////////////////////////////////////////////////////////
    var incrementPlus;
    var incrementMinus;
    var total;
    var buttonPlus = $(".cart-qty-plus");
    var buttonMinus = $(".cart-qty-minus");
    var num = 50;
    var index = 0;
    var result = 0;
    var vouch_count = 0;
    var incrementPlus = buttonPlus.click(function () {
        var $n = $(this)
            .parent(".button-container")
            .find(".qty");
        $n.val(++index);

        result = num * index;
        $('.res').val(result);
        $('.total').val(result + result2);
        $(".vouch_inp").val(index + index2);
    }); 

    var incrementMinus = buttonMinus.click(function () {
        var $n = $(this)
            .parent(".button-container")
            .find(".qty");

        if (Number($n.val()) > 0) {
            index = --index;
            $n.val(index)
            result = num * index;
            $('.res').val(result);
            $('.total').val(result + result2);
            $(".vouch_inp").val(index + index2);
        }
    });



    //----------------------------IKINCI--------------------------------------


    var buttonPlus2 = $(".cart-qty-plus2");
    var buttonMinus2 = $(".cart-qty-minus2");
    var result2 = 0;
    var num2 = 100;
    var index2 = 0;
    var incrementPlus = buttonPlus2.click(function () {
        var $n = $(this)
            .parent(".button-container")
            .find(".qty");
        $n.val(++index2);
        result2 = num2 * index2;
        $('.res2').val(result2);
        $('.total').val(result + result2);
        $(".vouch_inp").val(index + index2);
    });

    var incrementMinus = buttonMinus2.click(function () {
        var $n = $(this)
            .parent(".button-container")
            .find(".qty");

        if (Number($n.val()) > 0) {
            index2 = --index2;
            $n.val(index2)
            result2 = num2 * index2;
            $('.res2').val(result2);
            $('.total').val(result + result2);
            $(".vouch_inp").val(index + index2);
        }
    });

})